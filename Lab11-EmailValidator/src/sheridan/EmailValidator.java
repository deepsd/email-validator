package sheridan;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class EmailValidator {

	public static boolean isValidEmail(String email){

		String emailRegex = "^[^0-9]+[a-z0-9]{2,}+@{1}+[a-z0-9]{3,}+.+[a-z]{2,}$";
        Pattern pat = Pattern.compile(emailRegex);
        if(email==null){
            return false;
        }
        return pat.matcher(email).matches();

	}	
	
	 public static void main(String[] args)
	    {
			Logger logger  = Logger.getLogger(EmailValidator.class.getName()); 

	        String email = "shah98@@sheridancollege.ca";
	        if (isValidEmail(email)) {
	            logger.log(Level.INFO,"Yes");
	        }else{
	            logger.log(Level.INFO,"No");
	        }

	    }
}
