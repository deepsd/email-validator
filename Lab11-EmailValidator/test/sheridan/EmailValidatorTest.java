package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testIsValidEmailRegular() {
		boolean result = EmailValidator.isValidEmail("shah98@sheridancollege.ca");
		assertTrue("Invalid Email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionNull() {
		boolean result = EmailValidator.isValidEmail(null);
		assertFalse("Invalid Email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionBlank() {
		boolean result = EmailValidator.isValidEmail("");
		assertFalse("Invalid Email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionWrongFormat() {
		boolean result = EmailValidator.isValidEmail("sss");
		assertFalse("Invalid Email", result);
	}
	
	
	@Test
	public void testIsValidEmailExceptionAccountNameStartingWithNumber() {
		boolean result = EmailValidator.isValidEmail("1shah98@sheridancollege.ca");
		assertFalse("Invalid Email", result);
	}

	
	@Test
	public void testIsValidEmailExceptionUpperCase() {
		boolean result = EmailValidator.isValidEmail("SHAH@sheridancollege.ca");
		assertFalse("Invalid Email", result);
	}
	
	@Test
	public void testIsValidEmailExceptionExtensionNameWithNumber() {
		boolean result = EmailValidator.isValidEmail("shah98@sheridancollege.ca1");
		assertFalse("Invalid Email", result);
	}
	
	@Test
	public void testIsValidEmailBoundaryInAccountName() {
		boolean result = EmailValidator.isValidEmail("sha@sheridancollege.ca");
		assertTrue("Invalid Email", result);
	}
	
	@Test
	public void testIsValidEmailBoundaryInDomainName() {
		boolean result = EmailValidator.isValidEmail("sha98@she.ca");
		assertTrue("Invalid Email", result);
	}
	
	@Test
	public void testIsValidEmailBoundaryInExtensionName() {
		boolean result = EmailValidator.isValidEmail("sha98@sheridancollege.ca");
		assertTrue("Invalid Email", result);
	}

	@Test
	public void testIsValidEmailBoundaryInOnlyOneSymbol() {
		boolean result = EmailValidator.isValidEmail("sha98@sheridancollege.ca");
		assertTrue("Invalid Email", result);

	}

	@Test
	public void testIsValidEmailBoundaryOutAccountName() {
		boolean result = EmailValidator.isValidEmail("sh@sheridancollege.ca");
		assertFalse("Invalid Email", result);

	}
	
	@Test
	public void testIsValidEmailBoundaryOutDomainName() {
		boolean result = EmailValidator.isValidEmail("sha98@sh.ca");
		assertFalse("Invalid Email", result);

	}
	@Test
	public void testIsValidEmailBoundaryOutExtensionName() {
		boolean result = EmailValidator.isValidEmail("sha98@sheridancollege.c");
		assertFalse("Invalid Email", result);

	}

	@Test
	public void testIsValidEmailBoundaryOutOnlyOneSymbol() {
		boolean result = EmailValidator.isValidEmail("sha98@@sheridancollege.ca");
		assertFalse("Invalid Email", result);
	}

	
}
